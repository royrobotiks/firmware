#pragma once
/*! enumeration of client characteristic configuration descriptors */
enum
{
  BLE_GATT_SC_CCC_IDX,                    /*! GATT service, service changed characteristic */
  BLE_BATT_LVL_CCC_IDX,                   /*! Battery service, battery level characteristic */
  BLE_ESS_TEMP_CCC_IDX,                   /*! Environmental sensing service, temperature characteristic */
  BLE_ESS_HUMI_CCC_IDX,                   /*! Environmental sensing service, humidity characteristic */
  BLE_ESS_PRES_CCC_IDX,                   /*! Environmental sensing service, pressure characteristic */
  BLE_NUM_CCC_IDX
};

