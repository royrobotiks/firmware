#include "extmod/modbluetooth.h"
#include "py/runtime.h"
#include <stdint.h>

const char *const not_implemented_message =
	"Not (yet) implemented on card10. See https://git.card10.badge.events.ccc.de/card10/firmware/-/issues/8";

static void raise(void)
{
	mp_raise_NotImplementedError(not_implemented_message);
}

// Enables the Bluetooth stack.
int mp_bluetooth_init(void)
{
	raise();
	return 0;
}

// Disables the Bluetooth stack. Is a no-op when not enabled.
void mp_bluetooth_deinit(void)
{
	raise();
}

// Returns true when the Bluetooth stack is enabled.
bool mp_bluetooth_is_enabled(void)
{
	raise();
	return false;
}

// Gets the MAC addr of this device in big-endian format.
void mp_bluetooth_get_device_addr(uint8_t *addr)
{
}

// Start advertisement. Will re-start advertisement when already enabled.
// Returns errno on failure.
int mp_bluetooth_gap_advertise_start(
	bool connectable,
	int32_t interval_us,
	const uint8_t *adv_data,
	size_t adv_data_len,
	const uint8_t *sr_data,
	size_t sr_data_len
) {
	raise();
	return 0;
}

// Stop advertisement. No-op when already stopped.
void mp_bluetooth_gap_advertise_stop(void)
{
	raise();
}

// Start adding services. Must be called before mp_bluetooth_register_service.
int mp_bluetooth_gatts_register_service_begin(bool append)
{
	raise();
	return 0;
}
// // Add a service with the given list of characteristics to the queue to be registered.
// The value_handles won't be valid until after mp_bluetooth_register_service_end is called.
int mp_bluetooth_gatts_register_service(
	mp_obj_bluetooth_uuid_t *service_uuid,
	mp_obj_bluetooth_uuid_t **characteristic_uuids,
	uint8_t *characteristic_flags,
	mp_obj_bluetooth_uuid_t **descriptor_uuids,
	uint8_t *descriptor_flags,
	uint8_t *num_descriptors,
	uint16_t *handles,
	size_t num_characteristics
) {
	raise();
	return 0;
}
// Register any queued services.
int mp_bluetooth_gatts_register_service_end()
{
	raise();
	return 0;
}

// Read the value from the local gatts db (likely this has been written by a central).
int mp_bluetooth_gatts_read(
	uint16_t value_handle, uint8_t **value, size_t *value_len
) {
	raise();
	return 0;
}
// Write a value to the local gatts db (ready to be queried by a central).
int mp_bluetooth_gatts_write(
	uint16_t value_handle, const uint8_t *value, size_t value_len
) {
	raise();
	return 0;
}
// Notify the central that it should do a read.
int mp_bluetooth_gatts_notify(uint16_t conn_handle, uint16_t value_handle)
{
	raise();
	return 0;
}
// Notify the central, including a data payload. (Note: does not set the gatts db value).
int mp_bluetooth_gatts_notify_send(
	uint16_t conn_handle,
	uint16_t value_handle,
	const uint8_t *value,
	size_t *value_len
) {
	raise();
	return 0;
}
// Indicate the central.
int mp_bluetooth_gatts_indicate(uint16_t conn_handle, uint16_t value_handle)
{
	raise();
	return 0;
}

// Resize and enable/disable append-mode on a value.
// Append-mode means that remote writes will append and local reads will clear after reading.
int mp_bluetooth_gatts_set_buffer(uint16_t value_handle, size_t len, bool append)
{
	raise();
	return 0;
}

// Disconnect from a central or peripheral.
int mp_bluetooth_gap_disconnect(uint16_t conn_handle)
{
	raise();
	return 0;
}

#if MICROPY_PY_BLUETOOTH_ENABLE_CENTRAL_MODE
// Start a discovery (scan). Set duration to zero to run continuously.
int mp_bluetooth_gap_scan_start(
	int32_t duration_ms, int32_t interval_us, int32_t window_us
) {
	raise();
	return 0;
}

// Stop discovery (if currently active).
int mp_bluetooth_gap_scan_stop(void)
{
	raise();
	return 0;
}

// Connect to a found peripheral.
int mp_bluetooth_gap_peripheral_connect(
	uint8_t addr_type, const uint8_t *addr, int32_t duration_ms
) {
	raise();
	return 0;
}

// Find all primary services on the connected peripheral.
int mp_bluetooth_gattc_discover_primary_services(uint16_t conn_handle)
{
	raise();
	return 0;
}

// Find all characteristics on the specified service on a connected peripheral.
int mp_bluetooth_gattc_discover_characteristics(
	uint16_t conn_handle, uint16_t start_handle, uint16_t end_handle
) {
	raise();
	return 0;
}

// Find all descriptors on the specified characteristic on a connected peripheral.
int mp_bluetooth_gattc_discover_descriptors(
	uint16_t conn_handle, uint16_t start_handle, uint16_t end_handle
) {
	raise();
	return 0;
}

// Initiate read of a value from the remote peripheral.
int mp_bluetooth_gattc_read(uint16_t conn_handle, uint16_t value_handle)
{
	raise();
	return 0;
}

// Write the value to the remote peripheral.
int mp_bluetooth_gattc_write(
	uint16_t conn_handle,
	uint16_t value_handle,
	const uint8_t *value,
	size_t *value_len,
	unsigned int mode
) {
	raise();
	return 0;
}
#endif
