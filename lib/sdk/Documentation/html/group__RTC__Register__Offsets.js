var group__RTC__Register__Offsets =
[
    [ "MXC_R_RTC_SEC", "group__RTC__Register__Offsets.html#gabf0da9eccd92c5ca44ce8db68738b909", null ],
    [ "MXC_R_RTC_SSEC", "group__RTC__Register__Offsets.html#ga0b804cf2902effaff65175a61d235de5", null ],
    [ "MXC_R_RTC_TODA", "group__RTC__Register__Offsets.html#ga215206e26eff1208f093e3fedfbaf35e", null ],
    [ "MXC_R_RTC_SSECA", "group__RTC__Register__Offsets.html#ga5498549b108e81e22ae0a9e710906059", null ],
    [ "MXC_R_RTC_CTRL", "group__RTC__Register__Offsets.html#gadc7531eecd760886122ea09834c53dcc", null ],
    [ "MXC_R_RTC_TRIM", "group__RTC__Register__Offsets.html#ga3daeaab87adcfc4638d566a39fb1f8d1", null ],
    [ "MXC_R_RTC_OSCCTRL", "group__RTC__Register__Offsets.html#ga4e8e37868969f014b848abe7f1b5131b", null ]
];