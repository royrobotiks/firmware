#pragma once

#include "epicardium.h"

/*!< \brief Service start handle. */
#define ESS_START_HDL 0x1000
/*!< \brief Service end handle. */
#define ESS_END_HDL (ESS_MAX_HDL - 1)

enum {
	/*!< \brief environmental sensing services service declaration */
	ESS_SVC_HDL = ESS_START_HDL,
	/*!< \brief temperature characteristic */
	ESS_TEMPERATURE_CH_HDL,
	ESS_TEMPERATURE_VAL_HDL,
	ESS_TEMP_CH_CCC_HDL,                  /*!< Temperature CCCD */
	/*!< \brief humidity characteristic */
	ESS_HUMIDITY_CH_HDL,
	ESS_HUMIDITY_VAL_HDL,
	ESS_HUMI_CH_CCC_HDL,                  /*!< Humidity CCCD */
	/*!< \brief pressure characteristic */
	ESS_PRESSURE_CH_HDL,
	ESS_PRESSURE_VAL_HDL,
	ESS_PRES_CH_CCC_HDL,                  /*!< Pressure CCCD */


	/*!< \brief Maximum handle. */
	ESS_MAX_HDL
};

void bleESS_ccc_update(void);
