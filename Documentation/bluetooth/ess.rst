Environmental Sensing Service
========================

.. warning::
    This service will be available in version v1.17

The Environmental Sensing Service (ESS) implements access to
the BME680 environmental sensor of the card10.

It provides:

- Temperature
- Relative humidity
- Pressure


If notifcations are enabled a measurement of all values is performed every 3 seconds. For each measurement a notification is sent for the characteristics which have notifications enabled.


A measurement can also be triggered by reading from a characteristic. A measurement takes roughly 200 ms. A notifciation will be sent to all characteristics which have notifications enabled except the one which was used to trigger the measurement.


BLE Service
-----------

- Service

  UUID: ``181A``

- Temperature characteristic:

  UUID: ``2A6E``
  read and notify

- Humidity characteristic:

  UUID: ``2A6F``
  read and notify

- Pressure characteristic:

  UUID: ``2A6D``
  read and notify

Temperature characteristic
---------------------------------

- 16 bit little endian value representing the measured temperature.

- Unit: 0.01 deg C


Humidity characteristic
---------------------------------

- 16 bit little endian value representing the measured relative humidity.

- Unit: 0.01%

Pressure characteristic
---------------------------------

- 32 bit little endian value representing the measured pressure.

- Unit: 0.1 Pa (0.001 hPa)
