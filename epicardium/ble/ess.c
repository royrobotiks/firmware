#include "ess.h"
#include "cccd.h"

#include "wsf_types.h"
#include "util/bstream.h"
#include "wsf_assert.h"
#include "att_api.h"
#include "app_api.h"

#include "epicardium.h"
#include "modules/log.h"
#include "modules/modules.h"

#include "FreeRTOS.h"
#include "timers.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <machine/endian.h>

/* clang-format off */

/* BLE UUID for ESS service*/
static const uint8_t UUID_svc[] = { UINT16_TO_BYTES(ATT_UUID_ENVIRONMENTAL_SENSING_SERVICE) };
static const uint16_t UUID_svc_len = sizeof(UUID_svc);

/* BLE UUID for temperature */
static const uint8_t UUID_char_temperature[] = {
	ATT_PROP_READ | ATT_PROP_NOTIFY,
	UINT16_TO_BYTES(ESS_TEMPERATURE_VAL_HDL),
	UINT16_TO_BYTES(ATT_UUID_TEMPERATURE)
};

/* BLE UUID for humidity */
static const uint8_t UUID_char_humidity[] = {
	ATT_PROP_READ | ATT_PROP_NOTIFY,
	UINT16_TO_BYTES(ESS_HUMIDITY_VAL_HDL),
	UINT16_TO_BYTES(ATT_UUID_HUMIDITY)
};

/* BLE UUID for pressure */
static const uint8_t UUID_char_pressure[] = {
	ATT_PROP_READ | ATT_PROP_NOTIFY,
	UINT16_TO_BYTES(ESS_PRESSURE_VAL_HDL),
	UINT16_TO_BYTES(ATT_UUID_PRESSURE)
};

static const uint16_t UUID_char_len = sizeof(UUID_char_temperature);

static uint8_t initTemperatureValue[] = { UINT16_TO_BYTES(0) };
static uint16_t initTemperatureLen = sizeof(initTemperatureValue);

static uint8_t initHumidityValue[] = { UINT16_TO_BYTES(0) };
static uint16_t initHumidityLen = sizeof(initHumidityValue);

static uint8_t initPressureValue[] = { UINT32_TO_BYTES(0) };
static uint16_t initPressureLen = sizeof(initPressureValue);

/* Temperature client characteristic configuration */
static uint8_t essValTempChCcc[] = {UINT16_TO_BYTES(0x0000)};
static const uint16_t essLenTempChCcc = sizeof(essValTempChCcc);

/* Humidity client characteristic configuration */
static uint8_t essValHumidityChCcc[] = {UINT16_TO_BYTES(0x0000)};
static const uint16_t essLenHumidityChCcc = sizeof(essValHumidityChCcc);

/* Pressure client characteristic configuration */
static uint8_t essValPressureChCcc[] = {UINT16_TO_BYTES(0x0000)};
static const uint16_t essLenPressureChCcc = sizeof(essValPressureChCcc);

/* clang-format on */

/*
 * BLE service description
 */

static const attsAttr_t ESSSvcAttrList[] = {
	{
		.pUuid       = attPrimSvcUuid,
		.pValue      = (uint8_t *)UUID_svc,
		.pLen        = (uint16_t *)&UUID_svc_len,
		.maxLen      = sizeof(UUID_svc),
		.permissions = ATTS_PERMIT_READ,
	},

	// Temperature
	{
		.pUuid       = attChUuid,
		.pValue      = (uint8_t *)UUID_char_temperature,
		.pLen        = (uint16_t *)&UUID_char_len,
		.maxLen      = sizeof(UUID_char_temperature),
		.permissions = ATTS_PERMIT_READ,
	},
	{
		.pUuid       = attTemperatureChUuid,
		.pValue      = initTemperatureValue,
		.pLen        = &initTemperatureLen,
		.maxLen      = sizeof(initTemperatureValue),
		.settings    = ATTS_SET_READ_CBACK,
		.permissions = ATTS_PERMIT_READ | ATTS_PERMIT_READ_ENC |
			       ATTS_PERMIT_READ_AUTH,
	},
	/* Characteristic CCC descriptor */
	{
		.pUuid    = attCliChCfgUuid,
		.pValue   = essValTempChCcc,
		.pLen     = (uint16_t *)&essLenTempChCcc,
		.maxLen   = sizeof(essValTempChCcc),
		.settings = ATTS_SET_CCC,
		.permissions =
			(ATTS_PERMIT_READ |
			 ATTS_PERMIT_WRITE) // How about security?
	},

	// Humidity
	{
		.pUuid       = attChUuid,
		.pValue      = (uint8_t *)UUID_char_humidity,
		.pLen        = (uint16_t *)&UUID_char_len,
		.maxLen      = sizeof(UUID_char_humidity),
		.permissions = ATTS_PERMIT_READ,
	},
	{
		.pUuid       = attHumidityChUuid,
		.pValue      = initHumidityValue,
		.pLen        = &initHumidityLen,
		.maxLen      = sizeof(initHumidityValue),
		.settings    = ATTS_SET_READ_CBACK,
		.permissions = ATTS_PERMIT_READ | ATTS_PERMIT_READ_ENC |
			       ATTS_PERMIT_READ_AUTH,
	},
	/* Characteristic CCC descriptor */
	{
		.pUuid    = attCliChCfgUuid,
		.pValue   = essValHumidityChCcc,
		.pLen     = (uint16_t *)&essLenHumidityChCcc,
		.maxLen   = sizeof(essValHumidityChCcc),
		.settings = ATTS_SET_CCC,
		.permissions =
			(ATTS_PERMIT_READ |
			 ATTS_PERMIT_WRITE) // How about security?
	},

	// Pressure
	{
		.pUuid       = attChUuid,
		.pValue      = (uint8_t *)UUID_char_pressure,
		.pLen        = (uint16_t *)&UUID_char_len,
		.maxLen      = sizeof(UUID_char_pressure),
		.permissions = ATTS_PERMIT_READ,
	},
	{
		.pUuid       = attPressureChUuid,
		.pValue      = initPressureValue,
		.pLen        = &initPressureLen,
		.maxLen      = sizeof(initPressureValue),
		.settings    = ATTS_SET_READ_CBACK,
		.permissions = ATTS_PERMIT_READ | ATTS_PERMIT_READ_ENC |
			       ATTS_PERMIT_READ_AUTH,
	},
	/* Characteristic CCC descriptor */
	{
		.pUuid    = attCliChCfgUuid,
		.pValue   = essValPressureChCcc,
		.pLen     = (uint16_t *)&essLenPressureChCcc,
		.maxLen   = sizeof(essValPressureChCcc),
		.settings = ATTS_SET_CCC,
		.permissions =
			(ATTS_PERMIT_READ |
			 ATTS_PERMIT_WRITE) // How about security?
	},

};

// validating that the service really has all charateristics
WSF_CT_ASSERT(
	((sizeof(ESSSvcAttrList) / sizeof(ESSSvcAttrList[0])) ==
	 ESS_END_HDL - ESS_START_HDL + 1));

static TimerHandle_t poll_timer;
static StaticTimer_t poll_timer_buffer;
static void bleESS_update(struct bme680_sensor_data *data);

static void workpoll(void *data)
{
	struct bme680_sensor_data sensor_data;
	if (epic_bme680_read_sensors(&sensor_data) == 0) {
		bleESS_update(&sensor_data);
	}
}

static void poll(TimerHandle_t xTimer)
{
	workqueue_schedule(workpoll, NULL);
}

static void periodic(int period)
{
	if (poll_timer == NULL) {
		poll_timer = xTimerCreateStatic(
			"bme680_poll",
			1,
			pdTRUE,
			NULL,
			poll,
			&poll_timer_buffer
		);
	}

	if (period < 1) {
		xTimerStop(poll_timer, 0);
	} else {
		xTimerChangePeriod(poll_timer, pdMS_TO_TICKS(period), 0);
	}
}

static void
sendNotification(dmConnId_t connId, uint16_t handle, uint16_t cccidx)
{
	if (connId != DM_CONN_ID_NONE) {
		uint16_t len;
		uint8_t *value;
		uint8_t ret = AttsGetAttr(handle, &len, &value);
		if (ret == ATT_SUCCESS) {
			if (AttsCccEnabled(connId, cccidx)) {
				AttsHandleValueNtf(connId, handle, len, value);
			}
		}
	}
}

static void setAttrFromBME680(struct bme680_sensor_data *data)
{
	int16_t temperature;
	uint16_t humidity;
	uint32_t pressure;

	temperature = data->temperature * 100;
	AttsSetAttr(
		ESS_TEMPERATURE_VAL_HDL,
		sizeof(temperature),
		(uint8_t *)&temperature
	);

	humidity = data->humidity * 100;
	AttsSetAttr(
		ESS_HUMIDITY_VAL_HDL, sizeof(humidity), (uint8_t *)&humidity
	);

	pressure = data->pressure * 1000;
	AttsSetAttr(
		ESS_PRESSURE_VAL_HDL, sizeof(pressure), (uint8_t *)&pressure
	);
}

/*
 * BLE ESS read callback.
 *
 */
static uint8_t readESSCB(
	dmConnId_t connId,
	uint16_t handle,
	uint8_t operation,
	uint16_t offset,
	attsAttr_t *pAttr
) {
	struct bme680_sensor_data data;
	int ret = epic_bme680_read_sensors(&data);
	if (ret != 0) {
		return ATT_ERR_UNDEFINED;
	}
	setAttrFromBME680(&data);

	/* Send notifications (if enabled) for the _other_ characteristics. */
	switch (handle) {
	case ESS_TEMPERATURE_VAL_HDL:
		sendNotification(
			connId, ESS_HUMIDITY_VAL_HDL, BLE_ESS_HUMI_CCC_IDX
		);
		sendNotification(
			connId, ESS_PRESSURE_VAL_HDL, BLE_ESS_PRES_CCC_IDX
		);
		APP_TRACE_INFO1("ble-ess: read temperature: %d\n", temperature);
		return ATT_SUCCESS;
	case ESS_HUMIDITY_VAL_HDL:
		sendNotification(
			connId, ESS_TEMPERATURE_VAL_HDL, BLE_ESS_TEMP_CCC_IDX
		);
		sendNotification(
			connId, ESS_PRESSURE_VAL_HDL, BLE_ESS_PRES_CCC_IDX
		);
		APP_TRACE_INFO1("ble-ess: read humidity: %u\n", humidity);
		return ATT_SUCCESS;
	case ESS_PRESSURE_VAL_HDL:
		sendNotification(
			connId, ESS_TEMPERATURE_VAL_HDL, BLE_ESS_TEMP_CCC_IDX
		);
		sendNotification(
			connId, ESS_HUMIDITY_VAL_HDL, BLE_ESS_HUMI_CCC_IDX
		);
		APP_TRACE_INFO1("ble-ess: read pressure: %u\n", pressure);
		return ATT_SUCCESS;
	default:
		APP_TRACE_INFO0("ble-card10: read no handler found\n");
		return ATT_ERR_HANDLE;
	}
}

static attsGroup_t svcESSGroup = {
	.pNext       = NULL,
	.pAttr       = (attsAttr_t *)ESSSvcAttrList,
	.readCback   = readESSCB,
	.writeCback  = NULL,
	.startHandle = ESS_START_HDL,
	.endHandle   = ESS_END_HDL,
};

static void bleESS_update(struct bme680_sensor_data *data)
{
	setAttrFromBME680(data);

	/* Send notifications (if enabled) for all characteristics. */
	dmConnId_t connId = AppConnIsOpen();
	sendNotification(connId, ESS_TEMPERATURE_VAL_HDL, BLE_ESS_TEMP_CCC_IDX);
	sendNotification(connId, ESS_HUMIDITY_VAL_HDL, BLE_ESS_HUMI_CCC_IDX);
	sendNotification(connId, ESS_PRESSURE_VAL_HDL, BLE_ESS_PRES_CCC_IDX);
}

/*
 * This registers and starts the ESS service.
 */
void bleESS_init(void)
{
	AttsAddGroup(&svcESSGroup);
}

/*
 * Instruct the ESS service to check if any characterstics have
 * notifications enabled and enable/disable periodic measurements.
 */
void bleESS_ccc_update(void)
{
	dmConnId_t connId = AppConnIsOpen();
	if (connId != DM_CONN_ID_NONE &&
	    (AttsCccEnabled(connId, BLE_ESS_TEMP_CCC_IDX) ||
	     AttsCccEnabled(connId, BLE_ESS_HUMI_CCC_IDX) ||
	     AttsCccEnabled(connId, BLE_ESS_PRES_CCC_IDX))) {
		LOG_INFO("ess", "enable periodic measurement");
		periodic(3000);
	} else {
		LOG_INFO("ess", "disable periodic measurement");
		periodic(0);
	}
}
