#include "fs_util.h"

#include "epicardium.h"

#include <stdint.h>
#include <string.h>

int fs_read_file(char *filename, void *data, int len)
{
	int fd = epic_file_open(filename, "r");
	if (fd < 0) {
		return fd;
	}

	int res = epic_file_read(fd, data, len);
	epic_file_close(fd);

	return res;
}

int fs_read_text_file(char *filename, char *data, int len)
{
	int readbytes;

	if (len < 1)
		return -1;
	readbytes = fs_read_file(filename, data, len - 1);
	if (readbytes < 0) {
		data[0] = 0;
		return readbytes;
	};
	data[readbytes] = 0;
	while (readbytes > 0 && data[readbytes - 1] < 0x20) {
		data[--readbytes] = 0;
	};
	return readbytes;
}

int fs_write_file(char *filename, const void *data, int len)
{
	int fd = epic_file_open(filename, "w");
	if (fd < 0) {
		return fd;
	}

	int res = epic_file_write(fd, data, len);
	epic_file_close(fd);

	return res;
}

int fs_get_file_size(char *filename)
{
	struct epic_stat stat;
	int res = epic_file_stat(filename, &stat);
	return res < 0 ? res : (int)stat.size;
}

#if 0
#include "ff.h"
FATFS FatFs;          /* File system object for logical drive */
FS_USAGE FsUsage;
/* TODO: Port functions from r0ket/rad10 libs */
int fs_info(FATFS *fs)
{
    memcpy(fs, &FatFs, sizeof(FATFS));
    return 0;
}
int fs_usage(FATFS *fs, FS_USAGE *fs_usage)
{
    FRESULT res;
    DWORD tot_clust, fre_clust, sec_size;

    res = f_getfree("/", &fre_clust, &fs);
    if(res != FR_OK)
        return -res;

    // sectore size = sectors per cluster *  sector size
#if FF_MAX_SS == FF_MIN_SS
    sec_size = fs->csize * FF_MAX_SS;
#else
    sec_size = fs->csize * fs.ssize;
#endif

    // total/free sectors * sectore size
    tot_clust = fs->n_fatent - 2;
    fs_usage->total = tot_clust * sec_size; //FatFs.ssize;
    fs_usage->free = fre_clust * sec_size; //FatFs.ssize;

    return 0;
}
#endif
